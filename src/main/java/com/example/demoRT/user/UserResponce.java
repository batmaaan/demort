package com.example.demoRT.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponce {
    private int id;
    private String name;
    private String email;
    private String gender;
    private String status;
}
