package com.example.demoRT.create;

import com.example.demoRT.user.UserRequest;
import com.example.demoRT.user.UserResponce;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class CreateUser {

    public static void main(String args[]) {

        RestTemplate restTemplate = new RestTemplate();
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Bearer 29d177ce27fd1e5c2b7ade867dd0db2a9f155e3015dbcd4df2b4be2b2e901c98");
        final HttpEntity<UserRequest> requestHttpEntity = new HttpEntity<>(
                new UserRequest("Tenali Ramakrishna", "4444@15ce.com", "male", "active"), httpHeaders);

        final ResponseEntity<UserResponce> stringResponseEntity = restTemplate.postForEntity("https://gorest.co.in/public/v2/users", requestHttpEntity, UserResponce.class);
        final UserResponce body = stringResponseEntity.getBody();
        System.out.println(body);



    }
}
