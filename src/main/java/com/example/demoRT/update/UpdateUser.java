package com.example.demoRT.update;

import com.example.demoRT.user.User;
import com.example.demoRT.user.UserRequest;
import com.example.demoRT.user.UserResponce;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class UpdateUser {

    public static void main(String args[]) {

        RestTemplate restTemplate = new RestTemplate();
        final HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Bearer 29d177ce27fd1e5c2b7ade867dd0db2a9f155e3015dbcd4df2b4be2b2e901c98");
        User updatedUser = new User(5035, "Tenali UUUUU", "UUUU@15ce.com", "male", "active");
        final HttpEntity<User> requestHttpEntity = new HttpEntity<>(
                updatedUser, httpHeaders);

        final ResponseEntity<UserResponce> stringResponseEntity = restTemplate.exchange("https://gorest.co.in/public/v2/users/5035", HttpMethod.PUT, requestHttpEntity, UserResponce.class);
        final UserResponce body = stringResponseEntity.getBody();
        System.out.println(body);


    }
}
