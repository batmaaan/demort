package com.example.demoRT;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoRtApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoRtApplication.class, args);
	}

}
