package com.example.demoRT.getlist;

import com.example.demoRT.user.User;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

public class GetList {

    public static void main(String args[]) {


        RestTemplate restTemplate = new RestTemplate();
        User[] userArray = restTemplate.getForObject("https://gorest.co.in/public/v2/users", User[].class);
        final List<User> users = Arrays.asList(userArray);
        users.forEach(user -> {
            System.out.println("Id:    " + user.getId());
            System.out.println("Name:   " + user.getName());
            System.out.println("Email:   " + user.getEmail());
            System.out.println("Gender: " + user.getGender());
            System.out.println("Status: " + user.getStatus());
        });
    }
}
